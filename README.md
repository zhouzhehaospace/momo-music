# MomoMusic

#### 介绍

MOMO音乐项目，Vue3+vant+vite的移动端音乐播放器项目，接口采用网易云接口，UI设计参考腾讯MOO音乐。

补充说明：有些CSS样式没有调整好，但是部署时我这边改过来了。由于学业压力，很长时间无法更新。如果出现样式异常，还请自行修改SongList以及相关文件

#### 软件架构

软件架构说明

#### 安装教程

1. 从该仓库下载后，分别进入其中的两个文件夹，执行npm install

#### 使用说明

1. 在node的接口文件夹中，执行```node app.js```
2. 在MOMOMUSIC项目中，执行```npm run dev```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
