import request from './request';

// 首页推荐歌单
export const GetRecommendedSongList = limit => request.get(`/personalized?limit=${limit}`)
// 首页轮播推荐\
export const GetHomeSwipe = () => request.get('/banner?type=1')
// 推荐新歌
export const GetNewSongs = () => request.get('/personalized/newsong')
// 默认搜索关键词
export const GetDefaultKeyWord = () => request.get('/search/default')
// 歌曲详情
export const GetSongDetail = id => request.get(`/song/detail?ids=${id}`)
// 歌手信息(详情)
export const GetSingerData = id => request.get(`/artist/detail?id=${id}`)
// 歌曲搜索
export const SearchData = params => request.get(`/cloudsearch?keywords=${params}`)
// 获取歌单详情
export const GetSongListDetail = id => request.get(`/playlist/detail?id=${id}`)
// 获取歌词
export const GetLyric = id => request.get(`/lyric?id=${id}`)