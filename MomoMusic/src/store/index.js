import { createStore } from "vuex";

export default createStore({
    
    state:{
        playlist:[],
        playCurrentIndex:0,
        nowPlaying:{},
        isPlaying:false,
        playingId:1,
        lastPlayingId:2 ,
        lyric:[],
        currentTime:0,
        timeId:0,
        musicLong:'',
        singerImg:'',
    },

    mutations:{
        setPlayList:(state,value) => {
            state.playlist = value
        },
        setIsPlay:(state,value) => {
            state.isPlaying = value
        },
        setPlayingId:(state,value) => {
            state.lastPlayingId = state.playingId
            state.playingId = value
            state.isPlaying = false
        },
        setCurrentIndex:(state,value) => {
            state.playCurrentIndex = value
        },
        setLyric:(state,value) => {
            state.lyric = value
        },
        setCurrentTime:(state,value) => {
            state.currentTime = value
        },
        setMusicLong:(state,value) => {
            state.musicLong = value
        },
        setSingerId:(state,value) => {
            state.singerId = value
        },
        setSingerImg:(state,value) => {
            state.singerImg = value
        }
    },

    actions:{

    },

    modules:{

    },
    getters:{
        handleTime(){
            return (time) => {
                const crT = Math.floor(time)
                const timeSecond = crT % 60
                let TimeSecond = Math.round(timeSecond)
                if(TimeSecond < 10) {
                    TimeSecond = '0' + TimeSecond
                }
                const timeMinute = crT / 60
                let TimeMinute = parseInt(timeMinute)
                if(TimeMinute < 10) {
                    TimeMinute = '0' + TimeMinute
                }
                return TimeMinute + ':' + TimeSecond
            }
        },

        handleLyric(state){
            const arr = state.lyric.map((item,index) => {
                const min = parseInt(item.slice(1,3))
                const sec = parseInt(item.slice(4, 6))
                const mil = parseInt(item.slice(7, 10))
                const time = min * 60 * 1000 + sec * 1000 + mil
                // content = item.slice(11, item.length)
                const content = item.split(']')[1]
                return {
                    content: content,
                    min: min,
                    sec: sec,
                    mil: mil,
                    time: time,
                    item: item
                }

            })
            // 判断歌词到哪了
            arr.forEach((item, i) => {
                if (i == 0) {
                  item.pre = 0;
                } else {
                  item.pre = arr[i - 1].time;
                }
            })

            const newArr = [...arr]

            newArr.forEach((item,i) => {
                item.pre = item.time
                if(i + 1 > newArr.length - 1){
                    item.time = newArr[i].time
                }else{
                    item.time = newArr[i + 1].time
                }
            })
            return newArr
        }
    }
})