import { createApp } from 'vue'
import App from './App.vue'
import 'vant/lib/index.css'
import vant from 'vant'
import 'amfe-flexible'
import store from './store'
import router from './router/router'
import VTouch from 'vue3-touch';
import 'default-passive-events';
const app = createApp(App)
app.use(vant).use(router).use(store).use(VTouch)
app.mount('#app')
