import { createRouter,createWebHashHistory } from 'vue-router'
// 路由规则
const routes = [
    {
        path:'/',
        redirect:'/home'
    },
    {
        path:'/home',
        name:'home',
        component:() => import('../views/Home.vue'),
    },
    {
        path:'/searchPage',
        name:'searchPage',
        component:() => import('../views/SearchPage.vue')
    },
    {
        path:'/login',
        name:'login',
        component:() => import('../views/Login.vue')
    },
    {
        path:'/detail',
        name:'detail',
        component:() => import('../views/SongDetail.vue'),
    },

    {
        path:'/options',
        name:'options',
        component:() => import('../views/Options.vue')
    },
    {
        path:'/listView',
        name:'listView',
        component:() => import('../views/ListView.vue') 
    }
]
// 创建路由实例
const router = createRouter({
    // createWebHistory
    history:createWebHashHistory(),
    routes,
})
export default router;